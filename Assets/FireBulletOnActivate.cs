using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;  // Fixed the typo here

public class FireBulletOnActivate : MonoBehaviour
{
    public GameObject bullet;
    public Transform spawnPoint;
    public float fireSpeed = 20;

    void Start()
    {
        XRGrabInteractable grabbable = GetComponent<XRGrabInteractable>();
        grabbable.activated.AddListener(FireBullet);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void FireBullet(ActivateEventArgs arg)  // Added this method
    {
        GameObject spawnedBullet = Instantiate(bullet);  // Fixed the typo here
        spawnedBullet.transform.position = spawnPoint.position;  // Fixed the case in 'transform'
        Vector3 shootDirection = transform.forward;  // Assuming you want to shoot in the forward direction
        spawnedBullet.GetComponent<Rigidbody>().velocity = shootDirection * fireSpeed;
        Destroy(spawnedBullet, 5);
    }
}
